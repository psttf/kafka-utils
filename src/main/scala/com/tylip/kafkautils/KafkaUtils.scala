package com.tylip.kafkautils

import cats.effect.Async
import cats.effect.kernel.Sync

import java.time.Instant

//import cats.effect.{Blocker, ConcurrentEffect, ContextShift, Resource, Timer}
import cats.effect.Resource
import cats.instances.list._
import cats.syntax.flatMap._
import cats.syntax.functor._
import fs2.kafka._
import org.typelevel.log4cats.slf4j.Slf4jLogger
import mouse.boolean._
import pureconfig.ConfigSource
import pureconfig.module.catseffect.syntax._

import scala.concurrent.duration._

object KafkaUtils {

  def copyUntilResource[F[_]: Async](
    params: CopyTopicParams,
  ): Resource[F, fs2.Stream[F, Unit]] =
    for {
//      blocker <- Blocker[F]
      config <-
        Resource.eval(ConfigSource.default.loadF[F, KafkaUtilsConfig]())
      consumer <-
        KafkaConsumer.resource(
          ConsumerSettings(Deserializer.identity, Deserializer.identity)
            .withProperties(config.kafka.properties)
            .withGroupId("transfer-2020-11-18-20-38-Z")
            .withAutoOffsetReset(AutoOffsetReset.Earliest)
            .withEnableAutoCommit(false),
        )
      producer <-
        KafkaProducer.resource(
          ProducerSettings(Serializer.identity, Serializer.identity)
            .withProperties(config.kafka.properties),
        )
      logger <- Resource.eval(Slf4jLogger.create)
      _      <- Resource.eval(consumer.subscribeTo(params.sourceTopic))
      _      <- Resource.eval(logger.debug("hello"))
    } yield
      consumer.stream.chunks
        .map(chunk =>
          for {
            _ <- logger.info(s"chunk.size = ${chunk.size}")
            pair <-
              chunk.toList.foldLeft(
                Sync[F].pure(
                  List.empty[ConsumerRecord[Array[Byte], Array[Byte]]],
                  List.empty[ConsumerRecord[Array[Byte], Array[Byte]]],
                ),
              )((result, committableRecord) =>
                result.flatMap { case (toSkip, toTransfer) =>
                  Sync[F].fromEither(
                    createdInstant(committableRecord.record)
                      .toRight(
                        new Exception(
                          s"no created time for offset ${committableRecord.offset}",
                        ),
                      )
                      .map(instant =>
                        params.splitMoment
                          .forall(instant.isBefore).fold(
                            (toSkip, toTransfer :+ committableRecord.record),
                            (toSkip :+ committableRecord.record, toTransfer),
                          ),
                      ),
                  )
                },
              )
            (toSkip, toTransfer) = pair
            _ <- producer
              .produce(
                ProducerRecords(
                  toTransfer.map(record =>
                    ProducerRecord(
                      params.targetTopic,
                      record.key,
                      record.value,
                    ),
                  ),
                ),
              ).flatten.as(())
            _ <- logger.info(
              s"transferred: ${toTransfer.map(offsetInstant)}",
            )

            _ <-
              logger.info(
                s"skipped: ${toSkip.map(offsetInstant)}",
              )
          } yield chunk.map(_.offset),
        )
        .flatMap(fs2.Stream.evalUnChunk)
        .through(commitBatchWithin(1000, 10.seconds))

  private def offsetInstant(
    record: ConsumerRecord[Array[Byte], Array[Byte]],
  ) =
    record.offset -> createdInstant(record)

  private def createdInstant(
    record: ConsumerRecord[Array[Byte], Array[Byte]],
  ): Option[Instant] =
    record.timestamp.createTime.map(Instant.ofEpochMilli)

}
