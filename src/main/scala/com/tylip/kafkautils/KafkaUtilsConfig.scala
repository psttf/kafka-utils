package com.tylip.kafkautils

import pureconfig.ConfigReader
import pureconfig.generic.semiauto._

final case class KafkaConfig(properties: Map[String, String])

object KafkaConfig {
  implicit val kafkaConfigReader: ConfigReader[KafkaConfig] =
    deriveReader[KafkaConfig]
}

final case class KafkaUtilsConfig(kafka: KafkaConfig)

object KafkaUtilsConfig {
  implicit val kafkaUtilsConfigReader: ConfigReader[KafkaUtilsConfig] =
    deriveReader[KafkaUtilsConfig]
}
